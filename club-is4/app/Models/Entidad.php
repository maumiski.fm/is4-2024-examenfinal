<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Entidad extends Model
{
    protected $table = 'entidades';
    protected $primaryKey = 'identidad';

    public $incrementing = true;
    protected $keyType = 'integer';
    public $timestamps = false;

    protected $fillable = [
        'nombre', 'apellido', 'cedula', 'fechanacimiento',
        'email', 'celular', 'fechaalta', 'estado'
    ];

    public function paciente()
    {
        return $this->hasOne(Paciente::class, 'identidad');
    }

    public function accesos()
    {
        return $this->hasMany(Acceso::class, 'identidad');
    }

    public function medico()
    {
    return $this->hasOne(Medico::class, 'identidad');
    }

    // Otras relaciones y métodos que puedas necesitar

}
