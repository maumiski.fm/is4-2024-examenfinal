<?php

namespace App\Models;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Tymon\JWTAuth\Contracts\JWTSubject;

class User extends Authenticatable implements JWTSubject
{
    use Notifiable;

    protected $table = 'usuarios'; // Nombre de la tabla en la base de datos
    protected $primaryKey = 'identidad'; // Clave primaria en minúsculas

    public $incrementing = true; // Si la clave primaria es un número autoincrementable
    protected $keyType = 'integer'; // Tipo de la clave primaria

    public $timestamps = false; // Si tu tabla no usa campos timestamps como created_at y updated_at

    protected $fillable = [
        'clave' // Asegúrate de que el nombre coincida con la columna en tu tabla
    ];

    protected $hidden = [
        'clave' // Ocultar la contraseña en las respuestas JSON
    ];

    // Implementar los métodos requeridos por JWTSubject
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    public function getJWTCustomClaims()
    {
        return [];
    }

    // Relación con la tabla 'entidades'
    public function entidad()
    {
        return $this->belongsTo(Entidad::class, 'identidad'); // Referencia en minúsculas
    }

    

    // Si necesitas más métodos o lógica específica, puedes agregarlos aquí
}


