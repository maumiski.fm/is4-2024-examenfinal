<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Entidad;
use Illuminate\Support\Facades\Hash;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Facades\JWTAuth;
use Illuminate\Support\Facades\DB;

class AuthController extends Controller
{
    public function login(Request $request)
    {
        try {
            $request->validate([
                'cedula' => 'required|numeric', // Añadido mensaje para cedula
                'clave' => 'required',   // Eliminada la validación para que sea una cadena de caracteres
            ], [
                'cedula.required' => 'El campo cédula es obligatorio.',
                'cedula.numeric' => 'El campo cédula debe ser un número.',
                'clave.required' => 'El campo clave es obligatorio.',
            ]);

            // Buscar la entidad por cedula.
            $entidad = Entidad::where('cedula', $request->cedula)->first(); // Modificado a minúsculas

            // Si no se encuentra la entidad, devuelve un error.
            //if (!$entidad) {
                //return response()->json(['error' => 'Cedula no encontrada'], 401);
            //}

            // Encuentra el usuario relacionado con esa entidad.
            $user = User::where('identidad', $entidad->identidad)->first(); // Modificado a minúsculas

            // Si no se encuentra el usuario o la contraseña no coincide, devuelve un error.
            if (!$user || !Hash::check($request->clave, $user->clave)) { // Modificado a minúsculas
                return response()->json(['errors' => 'Credenciales incorrectas'], 401);
            }

            // Registra el evento de inicio de sesión en la tabla Accesos
            $this->registerAccessEvent($user->identidad, 'Login');

            // Genera el token JWT.
            $token = JWTAuth::fromUser($user);

            return response()->json([
                'access_token' => $token,
                'token_type' => 'Bearer',
            ]);
        } catch (JWTException $e) {
            return response()->json(['errors' => 'Error al generar el token'], 500);
        } catch (\Exception $e) {
            // Verificar si es una instancia de ValidationException
            if ($e instanceof \Illuminate\Validation\ValidationException) {
                return response()->json([
                    'message' => 'Error de validación',
                    'errors' => $e->errors(),
                ], 422); // Código de estado HTTP 422 para errores de validación
            } else {
                return response()->json([
                    'message' => 'Error en el servidor',
                    //'errors' => $e->getMessage(),
                    'file' => $e->getFile(),
                    'line' => $e->getLine()
                ], 500);
            }
        }
    }

    public function logout()
    {
        try {
            // Registra el evento de cierre de sesión en la tabla Accesos
            $user = auth()->user();
            if ($user) {
                $this->registerAccessEvent($user->identidad, 'Logout');
            }

            JWTAuth::invalidate(JWTAuth::getToken());
            return response()->json(['message' => 'Sesión cerrada con éxito']);
        } catch (\Exception $e) {
            return response()->json(['errors' => 'Error al cerrar sesión: ' . $e->getMessage()], 500);
        }
    }

    private function registerAccessEvent($userIdentidad, $action)
    {
        DB::table('accesos')->insert([
            'identidad' => $userIdentidad,
            'accion' => $action,
            'fecha' => now(),
        ]);
    }
}