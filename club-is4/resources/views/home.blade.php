<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Med Reserva</title>
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" rel="stylesheet">
    <style>
        body, html {
            margin: 0;
            padding: 0;
            height: 100%;
            background-image: url("{{ asset('images/fut.jpg') }}");
            background-position: center center;
            background-size: cover;
        }

        .logo-image {
            width: 100px;
            height: 100px;
            display: block;
            margin: 20px auto;
            border-radius: 50%;
        }

        .semi-transparent {
            background-color: rgba(255, 255, 255, 0.8);
            border-radius: 10px;
            padding: 3% 3%;
            width:80%;
            margin-top:10px;
            text-align:center;
        }

        .btn-custom {
            border: 1px solid #edfdfd;
            border-radius: 23px;
            background-color: #9de9e5;
            color: #3b868a;
            font-size: 18px;
            font-family: "Lato";
            font-weight: 700;
        }

        .navbar {
            background-color: rgba(255, 255, 255, 0.8);
        }
        li a{
            color:black;
        }
    </style>
</head>

<body>
    <main class="container">
        <nav class="navbar navbar-expand-lg">
            <div class="container">
                <img class="logo-image" src="{{ asset('images/fut2.jpg') }}" alt="Descripción de la imagen">
                <h1 class="text-black">Club IS4</h1>
                <ul class="navbar-nav mx-auto">
                    <li class="nav-item">
                        <a class="nav-link" href="home">Home</a>
                    </li>
                    <li class="nav-item">
                        <button class="btn btn-custom" onclick="logout()">Logout</button>
                    </li>
                </ul>
            </div>
        </nav>
        <div class="semi-transparent">
            <h2>Listado de futbolistas del plantel</h2>

            <div id="alert-container"></div>

            <div class="opcion-1" style="display: block;">
                <div class="row">
                    <div class="col">
                    <div class="card">
                            <div class="card-body" style="max-height: 300px; overflow-y: auto;">
                                <table class="table">
                                    <thead class="sticky-top">
                                        <tr>
                                            <th class="sticky-left">Nombre</th>
                                            <th class="sticky-left">Apellido</th>
                                            <th class="sticky-left">Edad</th>
                                            <th class="sticky-left">Numero camiseta</th>
                                        </tr>
                                    </thead>
                                    <tbody id="resultTableBody">
                                        <!-- Aquí se insertarán las filas de datos -->
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
        <div id="message" class="text-success mt-3"></div>
        <div id="error-message" class="text-danger mt-3"></div>
        
    </main>
    <script>
        const token = localStorage.getItem('jwt_token');
        console.log(localStorage.getItem('jwt_token'));
          function logout() {
            // Realizar la solicitud de cierre de sesión al backend
            fetch('/api/logout', {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer ' + localStorage.getItem('jwt_token')
                },
            })
                .then(response => {
                    if (response.ok) {
                        // Cierre de sesión exitoso
                        localStorage.removeItem('jwt_token');
                        document.getElementById('message').innerText = 'Sesión cerrada con éxito';
                        window.location.href = '/'; // Redirige a la página de inicio o a donde sea necesario
                    } else {
                        // Error en el cierre de sesión
                        document.getElementById('error-message').innerText = 'Error al cerrar sesión';
                    }
                })
                .catch(error => {
                    // Error de red u otro error
                    document.getElementById('error-message').innerText = 'Error en el servidor';
                });
        }
        document.addEventListener('DOMContentLoaded', function () {
        
            // Función para mostrar alerta
            function showAlert(data) {
                const alertContainer = document.getElementById('alert-container');
                alertContainer.innerHTML = '';

                let alertClass = 'alert-success';
                let alertMessage;

                if (data.errors || (data.message && data.message.toLowerCase().includes('error'))) {
                    alertClass = 'alert-danger';

                    if (data.errors) {
                        alertMessage = Array.isArray(data.errors) ? data.errors.join('') : processErrors(data.errors);
                    } else {
                        alertMessage = data.message || 'Error en la operación';
                    }
                } else {
                    alertMessage = data.message || 'Operación exitosa';
                }

                const alertHtml = `<div class="alert ${alertClass}">${alertMessage}</div>`;
                alertContainer.innerHTML = alertHtml;
            }
            
            // Función para cargar la lista de futbolistas
            function loadFutbolistasList() {
                fetch('/api/accesos', {
                    headers: {
                        'Authorization': `Bearer ${token}`
                    }
                })
                .then(response => {
                    if (!response.ok) {
                        throw new Error(`HTTP error! Status: ${response.status}`);
                    }
                    return response.json();
                })
                .then(data => {
                    const tableBody = document.getElementById('accesosTableBody');
                    tableBody.innerHTML = '';
                    const alertContainer = document.getElementById('alert-container');
                    alertContainer.innerHTML = '';

                    if (Array.isArray(data.accesos)) {
                        data.accesos.forEach(acceso => {
                            const row = document.createElement('tr');
                            row.innerHTML = `
                                <td>${acceso.nombre}</td>
                                <td>${acceso.apellido}</td>
                                <td>${acceso.cedula}</td>
                                <td>${acceso.accion}</td>
                                <td>${acceso.fecha}</td>
                            `;
                            tableBody.appendChild(row);
                        });
                    } else {
                        console.error('La respuesta del servidor no tiene una propiedad "accesos" que sea una matriz:', data);
                    }
                })
                .catch(error => {
                    console.error('Error al cargar la lista de futbolistas:', error);
                    showAlert({ errors: ['Error al cargar la lista de futbolistas'] });
                });
            }

            // Cargar la lista de futbolistas al cargar la página
            loadFutbolistasList();
        });
    </script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
</body>

</html>
