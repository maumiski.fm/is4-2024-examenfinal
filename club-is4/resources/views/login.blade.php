<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Fut IS4 Login</title>
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" rel="stylesheet">
    <style>
        body, html {
            margin: 0;
            padding: 0;
            height: 100%;
            background-image: url("{{ asset('images/fut.jpg') }}");
            background-position: center center;
            background-size: cover;
        }

        .logo-image {
            width: 100px;
            height: 100px;
            display: block;
            margin: 20px auto;
            border-radius: 50%;
        }

        .semi-transparent {
            background-color: rgba(255, 255, 255, 0.8);
            border-radius: 10px;
            padding: 3% 5%;
        }

        .btn-custom {
            border: 1px solid #edfdfd;
            border-radius: 23px;
            background-color: #9de9e5;
            color: #3b868a;
            font-size: 18px;
            font-family: "Lato";
            font-weight: 700;
        }
    </style>
</head>

<body>
    <header class="text-center py-4">
        <h1 class="text-white">Club IS4</h1>
    </header>

    <main class="container">
        <div class="row">
            <div class="col-md-4 mx-auto">
                <div class="semi-transparent">
                <div id="alert-container" class="mt-3"></div>
                    <form onsubmit="login(event)">
                        <div class="form-group">
                            <label for="username">Usuario</label>
                            <input type="text" class="form-control" id="username" placeholder="Usuario">
                        </div>
                        <div class="form-group">
                            <label for="password">Contraseña</label>
                            <input type="password" class="form-control" id="password" placeholder="••••••••">
                        </div>
                        <button type="submit" class="btn btn-custom btn-block">Iniciar sesión</button>
                        
                        <!-- Espacio para mostrar errores -->
                        <div id="error-message" class="text-danger mt-3"></div>
                    </form>
                </div>
            </div>
        </div>
    </main>
    <script>
        function login(event) {
            event.preventDefault();
    
            // Función para mostrar alerta
            function showAlert(data) {
                const alertContainer = document.getElementById('alert-container');
                alertContainer.innerHTML = '';

                let alertClass = 'alert-success';
                let alertMessage;

                if (data.errors || (data.message && data.message.toLowerCase().includes('error'))) {
                    alertClass = 'alert-danger';

                    if (data.errors) {
                        alertMessage = Array.isArray(data.errors) ? data.errors.join('') : processErrors(data.errors);
                    } else {
                        alertMessage = data.message || 'Error en la operación';
                    }
                } else {
                    alertMessage = data.message || 'Operación exitosa';
                }

                const alertHtml = `<div class="alert ${alertClass}">${alertMessage}</div>`;
                alertContainer.innerHTML = alertHtml;
            }

            // Función para procesar errores de validación
            function processErrors(errors) {
                if (errors && typeof errors === 'object') {
                    return Object.values(errors).flat().join('<br>');
                }
                return errors;
            }
    
            // Obtener valores del formulario
            var username = document.getElementById('username').value;
            var password = document.getElementById('password').value;
            console.log(username);
            console.log(password);
    
            // Realizar la solicitud de inicio de sesión al backend
            $.post('/api/login', { cedula: username, clave: password })
                .done(function (data) {
                    // Inicio de sesión exitoso
                    localStorage.setItem('jwt_token', data.access_token);
                    window.location.href = '/home';
                })
                .fail(function (xhr) {
            // Mostrar errores en el frontend
            var errorMessage = 'Error en el servidor';

            if (xhr.responseJSON && xhr.responseJSON.errors) {
                errorMessage = processErrors(xhr.responseJSON.errors);
            } else if (xhr.responseJSON && xhr.responseJSON.message) {
                errorMessage = xhr.responseJSON.message;
            }

            $('#error-message').html(errorMessage);
        });
        }
      
    </script>
    
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
</body>

</html>
